# Dansk-norske tekster

Tekstene her ble digitalisert og transkribert i forbindelse med [Dokumentasjonsprosjektet](http://www.dokpro.uio.no/). Presentasjon fra [prosjektets sider](http://www.dokpro.uio.no/litteratur.html):

> Gjennom Dokumentasjonsprosjektet ble en stor del av bokmålsmaterialet ved Seksjon for leksikografi og målføregransking, Institutt for nordistikk og litteraturvitenskap, UiO digitalisert. Arkivmaterialet består bl.a. av flere seddelarkiv med ekserpter fra norske forfatterskap. I stedet for å digitalisere sedlene i forfatterarkivene, er tekstene skannet inn i sin helthet. Det skannede materialet omfatter tekster fra ca. 1550 til ca. 1900, de fleste fra 1800-tallet. I alt er det skannet inn ca. 60 000 boksider. Materialet omfatter verker av bl.a. Mattis Størssøn, Absalon Pederssøn Beyer, Dorothe Engelbretsdatter, Petter Dass, C.B. Tullin, Claus Fasting, Henrik Wergeland, Asbjørnsen og Moe, Camilla Collett, Bjørnstjerne Bjørnson, Jonas Lie, Alexander L. Kielland, Jacob Breda Bull, Hans Jæger, Sigbjørn Obstfelder og Ragnhild Jølsen. Dessuten er en del memoarlitteratur blitt digitalisert. 

Filene er resultat av arbeidet til: Professor Dag Gundersen, INL, UiO; Gordana Ilic Holen, INL; Øyvind Eide; Christian-Emil Ore.

## Format

Alle filene er i TEI P4 eller P5.

> Legg merke til at noen det er noen tilsynelatende duplikater Bjornson og BjornsonP5. Det har med TEI versjonene å gjøre (se tei-c.org). Alle tekstne finnes i versonen TEI P4. Noen forfattere er videreforedlet til versjon TEI P5. Det er gjerne de forfatterne som http://www.bokselskap.no/ har villet ha. Gå ogaå inn på http://www.bokselskap.no/ og se om det er flere tekster som er aktuelle for et historisk litterært korpus. Alle tekstene i der er i TEI P5 xml. (Christian-Emil Ore)

[Bokselskap](http://www.bokselskap.no/) har jobbet videre med tekstene og publisert noen av tekstene som `epub` og `mobi`.
